# docker-datadog-agent

This Terraform module helps to install and configure the Docker Datadog agent service container on a vps.

```
To successfully connect to a remote server the private key must be added to the local SSH agent.

Issues to resolve this problem:

- https://github.com/hashicorp/terraform-provider-docker/issues/259
- https://github.com/kreuzwerker/terraform-provider-docker/issues/29
```

## Resources

- https://registry.terraform.io/providers/kreuzwerker/docker
