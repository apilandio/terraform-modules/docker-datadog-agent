terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
      version = "2.11.0"
    }
  }
}

provider "docker" {
  host = "ssh://${var.ssh_user}@${var.ssh_host}:${var.ssh_port}"
}

data "docker_registry_image" "datadog_agent" {
  name = var.datadog_agent_docker_image
}

resource "docker_image" "datadog_agent" {
  name = data.docker_registry_image.datadog_agent.name
  pull_triggers = [
    data.docker_registry_image.datadog_agent.sha256_digest
  ]
}

resource "docker_service" "datadog_agent" {
  depends_on = [
    null_resource.datadog_create_run_directory
  ]

  name = "dd-agent"

  task_spec {
    restart_policy = {
      condition = "on-failure"
      max_attempts = 0
    }
    container_spec {
      image = docker_image.datadog_agent.latest

      env = {
        DD_SITE = var.datadog_site
        DD_API_KEY = var.datadog_api_key
        DD_LOGS_ENABLED = true
        DD_LOGS_CONFIG_CONTAINER_COLLECT_ALL = true
        DD_CONTAINER_EXCLUDE_LOGS = "name:datadog-agent"
      }

      mounts {
        type = "bind"
        read_only = true
        target = "/var/run/docker.sock"
        source = "/var/run/docker.sock"
      }

      mounts {
        type = "bind"
        read_only = true
        target = "/host/sys/fs/cgroup"
        source = "/sys/fs/cgroup/"
      }

      mounts {
        type = "bind"
        read_only = true
        target = "/host/proc/"
        source = "/proc/"
      }

      mounts {
        type = "bind"
        read_only = false
        target = "/opt/datadog-agent/run/"
        source = "/opt/datadog-agent/run/"
      }
    }
  }
  endpoint_spec {
    ports {
      target_port    = "8126"
      published_port = "8126"
    }
  }
}

resource "null_resource" "datadog_create_run_directory" {
  connection {
    type = "ssh"
    host = var.ssh_host
    user = var.ssh_user
    port = var.ssh_port
    private_key = file(var.ssh_private_key_filepath)
  }
  provisioner "remote-exec" {
    inline = [
      # directory must exist in order to run the container
      "mkdir -p /opt/datadog-agent/run"
    ]
  }
}
