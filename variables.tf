variable "ssh_host" {
  type = string
  description = "The SSH host"
}

variable "ssh_user" {
  type = string
  description = "The SSH user"
}

variable "ssh_port" {
  type = number
  default = 22
  description = "The SSH port"
}

variable "ssh_private_key_filepath" {
  type = string
  description = "The SSH private key file path"
}

variable "datadog_site" {
  type = string
  description = "The Datadog endpoint to connect to"
  default = "datadoghq.eu"
}

variable "datadog_api_key" {
  type = string
  description = "The Datadog api key"
}

variable "datadog_agent_docker_image" {
  type = string
  description = "The Datadog Docker image to use"
  default = "gcr.io/datadoghq/agent:7"
}
